# @chermnyx dotfiles

## Variables

| name                      | type               | description                                                                                         |
| ------------------------- | ------------------ | --------------------------------------------------------------------------------------------------- |
| `use_symlinks`            | `bool`             | Set true to use symlinks (like gnu stow) instead of copying the configs. Requires fully cloned repo |
| `extra_files`             | `{ [str]: str }[]` | list of mappings of src_folder : dest_folder that will be copied to remote host                     |
| `dotfiles_root`           | `str`              | where to copy dotfiles                                                                              |
| `dotfiles_root_become`    | `bool`             | install dotfiles as root                                                                            |
| `dotfiles_profile`        | `str`              | where to copy dotfiles .profile script                                                              |
| `dotfiles_profile_become` | `bool`             | install dotfiles_profile as root                                                                    |
| `dotfiles_repo_clone`     | `bool`             | true: clone full repo to `dotfiles_root`; false: copy only required files                           |
| `host_bg`                 | `str?`             | change shell host background to given color (use shell-compatible colors)                           |

## Groups

| name     | description                              |
| -------- | ---------------------------------------- |
| `core`   | Install only core (zsh and envvars)      |
| `system` | Install system configs (requires `core`) |

## Tags

| name    | description                                  |
| ------- | -------------------------------------------- |
| `clone` | clone the repo if dotfiles_repo_clone is set |

## Installation

1. Configure your inventory (see <./inventory.yml.example>)
2. Run `ansible-playbook playbooks/_dotfiles_install.yml`
