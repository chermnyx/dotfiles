- set_fact:
    ansible_user: '{{ ansible_user | default(ansible_env.USER) }}'
    HOME: '{{ ansible_env.HOME }}'
  become: false

- name: check config
  debug:
    msg:
      chsh_users: '{{ chsh_users }}'
      zshrc_paths: '{{ zshrc_paths }}'

- set_fact:
    chsh_users_r: |-
      {{ chsh_users
          | map('regex_replace', '\$USER', ansible_user)
          | map('regex_replace', '\$HOME', HOME)
          | list }}
    zshrc_paths_r: |-
      {{ zshrc_paths
          | map('regex_replace', '\$USER', ansible_user)
          | map('regex_replace', '\$HOME', HOME)
          | list }}

- name: check variables
  debug:
    msg:
      chsh_users_r: '{{ chsh_users_r }}'
      zshrc_paths_r: '{{ zshrc_paths_r }}'

- name: Install packages
  become: true
  ansible.builtin.package:
    name: '{{ item }}'
    state: present
  with_items:
    - zsh
    - tmux
    - git

- name: mkdir {{ ZSH }}
  become: true
  ansible.builtin.file:
    path: '{{ ZSH }}'
    mode: '0755'
    state: directory

- name: Clone oh-my-zsh
  become: true
  ansible.builtin.git:
    repo: https://github.com/ohmyzsh/ohmyzsh
    depth: 1
    dest: '{{ ZSH }}'
    umask: '0022'
- name: Clone plugin zsh-users/zsh-completions
  become: true
  ansible.builtin.git:
    repo: https://github.com/zsh-users/zsh-completions
    depth: 1
    dest: '{{ ZSH_CUSTOM }}/plugins/zsh-completions'
    umask: '0022'
- name: Clone plugin zsh-users/zsh-autosuggestions
  become: true
  ansible.builtin.git:
    repo: https://github.com/zsh-users/zsh-autosuggestions
    depth: 1
    dest: '{{ ZSH_CUSTOM }}/plugins/zsh-autosuggestions'
    umask: '0022'
- name: Clone plugin zsh-users/zsh-syntax-highlighting
  become: true
  ansible.builtin.git:
    repo: https://github.com/zsh-users/zsh-syntax-highlighting
    depth: 1
    dest: '{{ ZSH_CUSTOM }}/plugins/zsh-syntax-highlighting'
    umask: '0022'
- name: Clone theme https://github.com/romkatv/powerlevel10k
  become: true
  ansible.builtin.git:
    repo: https://github.com/romkatv/powerlevel10k
    depth: 1
    dest: '{{ ZSH_CUSTOM }}/themes/powerlevel10k'
    umask: '0022'

- name: Configure prompt theme
  become: true
  copy:
    src: p10k.zsh
    dest: '{{ p10k_path }}'
    mode: '755'
- name: Configure zshrc
  become: true
  template:
    src: zshrc.j2
    dest: '{{ item }}'
    mode: '755'
  loop: '{{ zshrc_paths_r }}'

- name: Set login shell of user `{{ item }}` to `{{ ZSH_BIN }}`
  become: true
  user:
    name: '{{ item }}'
    shell: '{{ ZSH_BIN }}'
  loop: '{{ chsh_users_r }}'
