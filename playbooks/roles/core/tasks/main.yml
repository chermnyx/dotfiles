- name: Install packages
  become: true
  ansible.builtin.package:
    name:
      - '{{ item }}'
    state: present
  # use loops instead of list of package to continue installation even if one of the packages could not be installed
  with_items:
    - git
    - subversion
    - tmux
    - fzf
    - bat
    - zsh

    - lf
    - ripgrep
    - fd
    - lsd
    - bat
    - fping

    - duf
    - dust
    - sd
    - git-delta
    - dog
    - procs
  ignore_errors: true

- name: Setup dotfiles git repo
  when: dotfiles_repo_clone
  tags:
    - clone
  block:
    - name: Get current git branch
      # noqa command-instead-of-module
      delegate_to: localhost
      ansible.builtin.command:
        cmd: git rev-parse --abbrev-ref HEAD
      register: _git_ref
      changed_when: false

    - name: Clone dotfiles repo
      when: dotfiles_repo_clone
      become: '{{ dotfiles_root_become }}'
      ansible.builtin.git:
        repo: git@gitlab.com:chermnyx/dotfiles
        dest: '{{ dotfiles_root }}'
        version: '{{ _git_ref.stdout }}'

- name: Create instllaton structure
  become: '{{ dotfiles_root_become }}'
  ansible.builtin.file:
    state: directory
    path: '{{ item }}'
    mode: '755'
  with_items:
    - '{{ dotfiles_root }}'
    - '{{ dotfiles_root }}/.static'
    - '{{ dotfiles_root }}/.static/vendor'

- name: Get tmux-themepack
  ansible.builtin.git:
    repo: https://github.com/jimeh/tmux-themepack
    dest: '{{ dotfiles_root }}/.static/vendor/tmux-themepack'
  become: '{{ dotfiles_root_become }}'

- name: Get zinit
  ansible.builtin.git:
    repo: https://github.com/zdharma-continuum/zinit
    dest: '{{ dotfiles_root }}/.static/vendor/zinit'
  become: '{{ dotfiles_root_become }}'

- name: Make sure bootstrap directory exists
  become: '{{ dotfiles_profile_become }}'
  ansible.builtin.file:
    path: "{{ dotfiles_profile | dirname }}"
    state: directory
    mode: '755'

- name: Create bootstrap config
  become: '{{ dotfiles_profile_become }}'
  ansible.builtin.template:
    src: files/bootstrap/00-dotfiles.sh
    dest: '{{ dotfiles_profile }}'
    mode: '755'

- name: ln bootstrap static
  when: use_symlinks
  ansible.builtin.import_tasks: dir.ln.yml
  become: '{{ dotfiles_root_become }}'
  vars:
    src: 'files/bootstrap/static'
    dest: '{{ dotfiles_root }}/.static'
    prefix: '{{ dotfiles_root }}/playbooks/roles/core/files/bootstrap/static'
- name: cp bootstrap static
  when: not use_symlinks
  ansible.posix.synchronize:
    src: 'files/bootstrap/static/'
    dest: '{{ dotfiles_root }}/.static/'
    checksum: true
    owner: false
    group: false
    times: false
  become: '{{ dotfiles_root_become }}'

- name: ln home configs
  when: use_symlinks
  ansible.builtin.import_tasks: dir.ln.yml
  vars:
    src: 'files/home'
    dest: '{{ ansible_env.HOME }}'
    prefix: '{{ dotfiles_root }}/playbooks/roles/core/files/home'
- name: cp home configs
  when: not use_symlinks
  ansible.posix.synchronize:
    src: home/
    dest: '{{ ansible_env.HOME }}/'
    checksum: true
    owner: false
    group: false
    times: false

- name: Copy extra files
  ansible.posix.synchronize:
    src: '../{{ _extra_file.key }}/'
    dest: '{{ _extra_file.value }}/'
    checksum: true
    owner: false
    group: false
    times: false
  with_items: '{{ extra_files | dict2items }}'
  loop_control:
    loop_var: _extra_file
