#!/bin/sh

export _dotfiles_env_installed=1
_dotfilesdir='{{ dotfiles_root }}'
export _dotfilesdir
_srcdir="${_dotfilesdir}/.static"
export _srcdir

_zplugins="${_srcdir}/zsh/plugins"

{% if host_bg is defined %}
export _DOTFILES_HOST_BG='{{ host_bg }}'
{% endif %}
