#!/bin/zsh

# module_path+=( "$HOME/.local/share/zinit/module/Src" )
# zmodload zdharma_continuum/zinit

source "${_srcdir}/vendor/zinit/zinit.zsh"
zicompinit

_dotfiles_install() {
	fast-theme XDG:overlay > /dev/null
	# bruh: https://github.com/zdharma-continuum/zinit-module/issues/4
	# zinit module build
}


zinit snippet "${_srcdir}/zsh/plugins/powerlevel9k-config.zsh"
zinit snippet "${_srcdir}/zsh/plugins/keybinds.zsh"

_omzp_line() {
	local _plugin_name="$1"
	echo pick"plugins/${_plugin_name}/${_plugin_name}.plugin.zsh" src"plugins/${_plugin_name}/" ohmyzsh/ohmyzsh
}

_omzl_line() {
	local _plugin_name="$1"
	echo pick"lib/${_plugin_name}.zsh" src"lib/" ohmyzsh/ohmyzsh

}

# should be the first plugin to load because of weird fzf-tab completion behavior
zinit ice pick"completion.zsh"; zinit load "${_srcdir}/zsh/plugins"

# system completions
_zsh_system_dir=/usr/share/zsh
zinit id-as=system-completions wait as=completion lucid \
	atclone='print Installing system completions...; \
	mkdir -p $ZPFX/funs; \
	command cp -f $_zsh_system_dir/functions/**/_* $_zsh_system_dir/site-functions/**/_* $ZPFX/funs; \
	zinit creinstall -q $_zsh_system_dir/functions; \
	zinit creinstall -q $_zsh_system_dir/site-functions' \
	atload='fpath=( ${(u)fpath[@]:#$_zsh_system_dir/*} ); \
	fpath+=( $ZPFX/funs )' \
	atpull="%atclone" run-atpull for \
		zdharma-continuum/null

# custom completions + omz
zinit light-mode lucid blockf for \
	zsh-users/zsh-completions \
	as"completeion" nix-community/nix-zsh-completions \
	\
	pick"oh-my-zsh.sh" ohmyzsh/ohmyzsh \
	\
	$(_omzp_line colored-man-pages) \
	$(_omzp_line command-not-found) \
	$(_omzp_line dirhistory) \
	$(_omzp_line docker-compose) \
	$(_omzp_line encode64) \
	$(_omzp_line extract) \
	$(_omzp_line fzf) \
	$(_omzp_line history) \
	$(_omzp_line kubectl) \
	$(_omzp_line jsontools) \
	$(_omzp_line perms) \
	$(_omzp_line sudo) \
	$(_omzp_line tmux) \
	$(_omzp_line urltools) \

zinit light-mode lucid blockf for \
	zsh-users/zsh-autosuggestions \
	zdharma-continuum/fast-syntax-highlighting \
	hlissner/zsh-autopair \
	wfxr/forgit \
	as"completeion" garabik/grc \
	as"program" pick"bin/preview.sh" junegunn/fzf.vim \

# `git forgit ...`
PATH="$PATH:$FORGIT_INSTALL_DIR/bin"

zinit light-mode lucid nocd blockf for \
	pick"fast-theme-config.zsh" "${_srcdir}/zsh/plugins" \
	pick"fzf-tab-preview.zsh" "${_srcdir}/zsh/plugins" \

# completion overrides & global aliases should be loaded in the last moment
zinit light-mode lucid nocd blockf for \
	pick"alias.zsh" "${_srcdir}/zsh/plugins" \

zinit from"gh-r" as"program" bpick"*linux*" mv"direnv* -> direnv" \
	atclone'./direnv hook zsh > zhook.zsh' atpull'%atclone' \
	pick"direnv" src="zhook.zsh" for \
	direnv/direnv

# needs to be loaded in sync mode; breaks custom prompts otherwise
zinit ice depth=1; zinit light romkatv/powerlevel10k

zicdreplay
