#!/usr/bin/env python3
import sys
from dataclasses import dataclass

assert len(sys.argv) > 1


@dataclass
class Screen:
    w: int
    h: int
    diag: float

    @property
    def d(self):
        return (self.w ** 2 + self.h ** 2) ** 0.5

    @property
    def dpi(self):
        return self.d / self.diag

    @property
    def dpi_scale(self):
        return self.dpi / primary_screen.dpi

    @property
    def fhd_diag(self):
        return primary_screen.diag / self.dpi_scale

    @property
    def delta(self):
        return self.fhd_diag - primary_screen.diag

    def __repr__(self):
        return f"<Screen {self.w}x{self.h}@{self.diag} {self.dpi:>6.2f}dpi>"


screens = []
for x in sys.argv[1:]:
    wh, diag = x.split("@")
    w, h = wh.split("x")

    screens.append(Screen(w=int(w), h=int(h), diag=float(diag)))

primary_screen = screens[0]
screens.sort(key=lambda x: abs(x.delta))

for screen in screens:
    print(
        f"{screen}"
        f" | 1080p diag: {screen.fhd_diag:>5.2f}"
        f" | delta: {screen.delta:>6.2f}"
        f" | dpi scale: {screen.dpi_scale:>5.4f}"
        f" | dpi scale inv: {1/screen.dpi_scale:>5.4f}"
    )
