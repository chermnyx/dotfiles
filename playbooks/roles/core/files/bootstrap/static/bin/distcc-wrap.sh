#!/bin/bash

while [[ "$1" == "distcc" ]]; do
	shift
done

# compiler="$(basename "$1")"
compiler="$1"
shift

exec distcc "$compiler" "$@"
