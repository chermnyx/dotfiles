#!/bin/env bash
echo HUGE_PAGES_GiB=$HUGE_PAGES_GiB
HUGE_PAGES_SIZE_kB=$(grep Hugepagesize /proc/meminfo | awk '{ print $2 }')

HUGE_PAGES_NUM=$((HUGE_PAGES_GiB * 1024 * 1024 / HUGE_PAGES_SIZE_kB))
case "$1" in
	alloc)
	while [[ $(grep Huge /proc/meminfo | grep HugePages_Total | awk '{print $2}') != $HUGE_PAGES_NUM ]]; do
		echo 3 | sudo tee /proc/sys/vm/drop_caches
		echo 1 | sudo tee /proc/sys/vm/compact_memory
		echo -n $(grep Huge /proc/meminfo | grep HugePages_Total | awk '{print $2}')/
		echo $HUGE_PAGES_NUM | sudo tee /sys/kernel/mm/hugepages/hugepages-${HUGE_PAGES_SIZE_kB}kB/nr_hugepages
	done
	;;

	dealloc)
		echo 0 | sudo tee /sys/kernel/mm/hugepages/hugepages-${HUGE_PAGES_SIZE_kB}kB/nr_hugepages
	;;
esac
