#!/usr/bin/python3
# -*- coding: utf-8 -*-
from blessings import Terminal
import colorama
colorama.init()
t = Terminal()

print(t.bright_yellow('-*-Color Test-*-'), '\n')
x = 'X~x'
a = t.normal
style = [t.bold, t.reverse, t.underline, t.blink]
color = [t.black, t.red, t.green, t.yellow, t.blue, t.magenta, t.cyan, t.white]
bg = [t.on_black, t.on_red, t.on_green, t.on_yellow,
      t.on_blue, t.on_magenta, t.on_cyan, t.on_white]
br_color = [t.bright_red, t.bright_green, t.bright_yellow,
            t.bright_blue, t.bright_magenta, t.bright_cyan]
s = color + br_color
x = 'KEK '
for i in bg:
    for j in s:
        print(i + j + x + a, end='')
    print()
print()
print('bold, reverse, underline, blink')
for i in style:
    for j in s:
        print(i + j + x + a, end='')
    print()
