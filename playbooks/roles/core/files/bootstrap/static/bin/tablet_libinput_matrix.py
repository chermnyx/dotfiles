#!/usr/bin/env python3
import click
from fractions import Fraction
from itertools import chain
from math import *


@click.command()
@click.argument('d', nargs=1, type=Fraction)
@click.argument('tablet_x_in', nargs=1, type=Fraction)
@click.argument('factor', nargs=1, type=Fraction)
@click.argument('tab_ax', nargs=1, type=Fraction)
@click.argument('tab_ay', nargs=1, type=Fraction)
@click.argument('ax', type=Fraction, default=16)
@click.argument('ay', type=Fraction, default=9)
@click.argument('offset_x_px', type=Fraction, default=0)
@click.argument('offset_y_px', type=Fraction, default=0)
def main(d, tablet_x_in, factor, tab_ax, tab_ay, ax, ay, offset_x_px, offset_y_px):

    # 1 => tablet_x_in
    # k => display_x_in * factor
    # k = display_x_in * factor / tablet_x_in
    x = sqrt(d ** 2 / (ax ** 2 + ay ** 2))

    size_factor = ax * x * factor / tablet_x_in

    # print(c_x, c_y)
    c0 = tab_ax / ax
    c2 = tab_ay / ay

    c0, c2 = c0 / c0, c2 / c0

    c0 /= size_factor
    c2 /= size_factor

    c0, c2 = (float(el) for el in (c0, c2))

    c1 = float(offset_x_px)
    c3 = float(offset_y_px)

    matrix = [[c0, 0, c1],
              [0, c2, c3],
              [0, 0, 1]]
    click.echo(' '.join(str(el) for line in matrix for el in line))


if __name__ == "__main__":
    main()
