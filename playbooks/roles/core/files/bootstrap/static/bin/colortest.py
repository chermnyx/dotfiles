#!/usr/bin/python3
# -*- coding: utf-8 -*-
import shutil
import math

tl = shutil.get_terminal_size((80, 20))[0]
# st = math.ceil(256 / tl)
# b8 = tl // 8
# cu = tl // 36
# gr = tl // 24

st = 2
b8 = 2
cu = 2
gr = 2


def pc(r, g, b):
    print(f'\x1b[48;2;{r};{g};{b}m', end=" ")


def reset():
    print("\x1b[0m")

print("Default: ")
for i in range(8):
    print(f'\x1b[4{i}m', end=' ' * b8)
reset()
for i in range(8, 16):
    print(f'\x1b[48;5;{i}m', end=' ' * b8)
reset()

print()
print("Grayscale")
for color in range(232, 257):
    print(f"\x1b[48;5;{color}m", end=' ' * gr)
reset()

print()
print('256 colors:')
for r in range(6):
    for g in range(6):
        for b in range(6):
            color = 16 + 36 * r + 6 * g + b
            print(f"\x1b[48;5;{color}m", end=' ' * cu)
        print("\x1b[0m", end='')
    print()
reset()

print()
print('TrueColor:')
for r in range(0, 255, st):
    pc(r, 0, 0)
reset()
for g in range(0, 255, st):
    pc(0, g, 0)
reset()
for b in range(0, 255, st):
    pc(0, 0, b)
reset()
