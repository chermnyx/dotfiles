#!/usr/bin/env python3
import click
from fractions import Fraction
from math import *


@click.command()
@click.argument('d', nargs=1, type=Fraction)
@click.argument('factor', nargs=1, type=Fraction)
@click.argument('cpix', nargs=1, type=Fraction)
@click.argument('cpiy', nargs=1, type=Fraction)
@click.argument('ax', type=Fraction, default=16)
@click.argument('ay', type=Fraction, default=9)
@click.option('-r', '--round', 'round_', default=True, type=bool)
def main(d, factor, cpix, cpiy, ax, ay, round_):
    x = sqrt(d**2 / (ax**2+ay**2))
    tab = (ax * x * factor * cpix, ay * x * factor * cpiy)
    if round_:
        tab = (round(tab[0]), round(tab[1]))
    click.echo(f'{tab[0]} {tab[1]}')


if __name__ == "__main__":
    main()
