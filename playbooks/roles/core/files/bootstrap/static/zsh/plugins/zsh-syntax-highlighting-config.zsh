#!/bin/zsh
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern)

ZSH_HIGHLIGHT_STYLES[bracket-error]='fg=red,bold'
ZSH_HIGHLIGHT_STYLES[bracket-level-1]='fg=blue,bold'
ZSH_HIGHLIGHT_STYLES[bracket-level-2]='fg=green,bold'
ZSH_HIGHLIGHT_STYLES[bracket-level-3]='fg=yellow,bold'
ZSH_HIGHLIGHT_STYLES[bracket-level-4]='fg=magenta,bold'
ZSH_HIGHLIGHT_STYLES[cursor-matchingbracket]='fg=gray,bold'

#ZSH_HIGHLIGHT_PATTERNS+=('rm *-rf*' 'fg=white,bold,bg=red')
#ZSH_HIGHLIGHT_PATTERNS+=('rm *-fr*' 'fg=white,bold,bg=red')
#ZSH_HIGHLIGHT_PATTERNS+=('rm *-r*' 'fg=white,bold,bg=red')

ZSH_HIGHLIGHT_STYLES[default]='none'
# unknown
#ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=red,bold'
# command
ZSH_HIGHLIGHT_STYLES[reserved-word]='fg=magenta,bold'
ZSH_HIGHLIGHT_STYLES[alias]='fg=green,bold'
ZSH_HIGHLIGHT_STYLES[builtin]='fg=green,bold'
ZSH_HIGHLIGHT_STYLES[function]='fg=green,bold'
ZSH_HIGHLIGHT_STYLES[command]='fg=green,bold'
ZSH_HIGHLIGHT_STYLES[precommand]='fg=green'
ZSH_HIGHLIGHT_STYLES[hashed-command]='fg=green,bold'
ZSH_HIGHLIGHT_STYLES[commandseparator]='fg=yellow,bold'
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='fg=cyan'
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='fg=cyan'
# path
#ZSH_HIGHLIGHT_STYLES[path]='fg=none,underline'
#ZSH_HIGHLIGHT_STYLES[path_prefix]='fg=none,underline'
#ZSH_HIGHLIGHT_STYLES[path_approx]='fg=none,underline'
# shell
ZSH_HIGHLIGHT_STYLES[globbing]='fg=yellow,underline'
ZSH_HIGHLIGHT_STYLES[history-expansion]='fg=blue,bold'
# ZSH_HIGHLIGHT_STYLES[assign]='fg=magenta,bold'
ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]='fg=cyan'
ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]='fg=cyan'
ZSH_HIGHLIGHT_STYLES[back-quoted-argument]='fg=cyan'
# quotes
# ZSH_HIGHLIGHT_STYLES[single-quoted-argument]='fg=none'
# ZSH_HIGHLIGHT_STYLES[double-quoted-argument]='fg=none'
