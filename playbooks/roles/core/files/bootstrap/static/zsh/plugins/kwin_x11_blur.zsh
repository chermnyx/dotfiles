#!/bin/zsh
if [[ -z "$SSH_CONNECTION" ]] && [[ "$UID" != 0 ]] && [[ -n "$DISPLAY" ]] && pidof kwin_x11 &>/dev/null; then
    for __term in yakuake tilix; do
        for i in $(xdotool search --pid $(pidof $__term) 2>/dev/null); do
            xprop -f _KDE_NET_WM_BLUR_BEHIND_REGION 32c -set _KDE_NET_WM_BLUR_BEHIND_REGION 0 -id $i
        done
    done
fi
