#!/bin/zsh
bindkey "\e[9;3~" backward-kill-word
#bindkey "\e[9;2~" backward-kill-line

bindkey "\e[3;5~" kill-word
#bindkey "\e[3;2~" kill-line

# bindkey '^[[5D' backward-word
# bindkey '^[[5C' forward-word

bindkey '^[[1;5C' forward-word
bindkey '^[[1;5D' backward-word

# bindkey ';5D' backward-word
# bindkey ';5C' forward-word

bindkey "^Z" undo
bindkey "^Y" redo
bindkey '^F' history-incremental-search-forward

# comment
bindkey '^_' vi-pound-insert

clear-widget() {
	clear
	zle reset-prompt
}
zle -N clear-widget
bindkey '^L' clear-widget

bindkey -s '^o' 'lfcd\n'

if [[ "$TERM" == 'linux' ]] || [[ "$COLORTERM" == "kmscon" ]]; then

else
	bindkey '^H' backward-kill-word
fi
