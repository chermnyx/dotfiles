unsetopt COMPLETE_ALIASES
setopt COMPLETE_IN_WORD

# zstyle ':completion:*' menu select=2
zstyle ':completion:*' list-dirs-first true
zstyle ':completion:*' sort false  # breaks some completions like git
# disable sort when completing options of any command
# zstyle ':completion:complete:*:options' sort false
# zstyle ':completion:*' file-sort name

# completion groups
zstyle ':completion:*' group-name ''

# autocorrection
zstyle ':completion:*' completer _complete _approximate

# completion descriptions
zstyle ':completion:*:options' description 'yes'
zstyle ':completion:*:options' auto-description '%d'
# zstyle ':completion:*:descriptions' format '> %B%d%b'
zstyle ':completion:*:descriptions' format '[%d]'

# Ignore completion functions for commands you don’t have:
zstyle ':completion:*:functions' ignored-patterns '_*'

# wait for new files in $PATH
zstyle ':completion:*' rehash true

unsetopt AUTO_LIST
setopt AUTO_MENU

# Built-in parsing of options output by --help
# compdef _gnu_generic fzf

if (( $+commands[kubectl] )); then
	source <(kubectl completion zsh)
fi

# if (( $+commands[kubectl-krew] )); then
# 	source <(kubectl krew completion zsh)
# fi

if (( $+commands[kubectl-argo-rollouts] )); then
	source <(kubectl argo rollouts completion zsh)
	compdef _kubectl-argo-rollouts kubectl-argo-rollouts
fi

if (( $+commands[yq] )) && (( $+commands[jq] )); then
	compdef yq=jq
fi

if (( $+commands[procs] )); then
	# zstyle ':completion:*:*:*:*:processes' command "procs --color=always --no-header"
	zstyle ':completion:*:*:*:*:processes' command "procs --no-header"
else
	zstyle ':completion:*:*:*:*:processes' command "ps aux"
fi

# switch group using `,` and `.`
# zstyle ':fzf-tab:*' switch-group ',' '.'
# zstyle ':fzf-tab:*' fzf-flags '--height=90%'
# zstyle ':fzf-tab:*' fzf-command ftb-tmux-popup
# zstyle ':fzf-tab:*' popup-pad 64 0
zinit load Aloxaf/fzf-tab
