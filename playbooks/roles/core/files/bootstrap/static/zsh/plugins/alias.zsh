#!/bin/zsh

# functions
my_sudo() {
	while [[ $# > 0 ]]; do
		case "$1" in
		command | exec)
			shift
			break
			;;
		nocorrect | noglob) shift ;;
		*) break ;;
		esac
	done
	if [[ $# == 0 ]]; then
		command sudo su
	else
		noglob command sudo $@
	fi
}

dotfiles-sync() {
	pushd "${_dotfilesdir}"

	set -x
	git pull

	PAGER=cat git diff
	git add -A
	git commit -m "changes from $(LC_ALL=en_US.UTF-8 uname -n) on $(LC_ALL=en_US.UTF-8 date)"
	git push

	set +x

	popd
}

exportf (){
	export "$(echo $1)"="`whence -f $1 | sed -e "s/$1 //" `"
}

fkill() {
	local pid
	pid=$(ps aux | sed 1d | fzf -m | awk '{print $2}')

	if [ "x$pid" != "x" ]; then
		echo $pid | xargs kill -${1:-9}
	fi
}

killzomb() {
	\kill $(\ps -A -ostat,ppid | awk '/[zZ]/{print $2}')
}


upd() {
	paru --pacman=/usr/bin/powerpill -Syu			"$@" &&
	#paru -Sy			"$@" &&
	#sudo powerpill -Su	"$@" &&
	#paru -Sua			"$@"

	if [ -z "$@" ] &&  (( $+commands[flatpak] )); then
		flatpak update
	fi &&

	if (( $+commands[tldr] )); then
		tldr --update
	fi &&

	if (( $+commands[rustup] )); then
		rustup update
	fi &&

	if (( $+commands[kubectl] )) && (( $+commands[kubectl-krew] )); then
		kubectl krew update &&
		kubectl krew upgrade
	fi
}

updf() {
	upd --noconfirm "$@" &&

	if (( $+commands[flatpak] )); then
		flatpak update -y
	fi
}

__fakeroot_msg() {
	print -P "%K{red}Don't use '%B$1%b' inside fakeroot\!$reset_color%k"
}

if (( $+commands[rga] )) && (( $+commands[fzf] )); then
	rga-fzf() {
		RG_PREFIX="rga --files-with-matches"
		local file
		file="$(
			FZF_DEFAULT_COMMAND="$RG_PREFIX '$1'" \
				fzf --sort --preview="[[ ! -z {} ]] && rga --pretty --context 5 {q} {}" \
					--phony -q "$1" \
					--bind "change:reload:$RG_PREFIX {q}" \
					--preview-window="70%:wrap"
		)" &&
		echo "opening $file" &&
		xdg-open "$file"
	}
	alias rga='noglob rga-fzf'
else
	if (( $+commands[rga] )); then
		alias rga='noglob rga'
	fi
fi

sshci() {
	ssh-copy-id -fi "$(ssh -G "$1" | grep "^identityfile " | head -1 | cut -d " " -f2- | sed "s|^~|$HOME|")" "$1"
}
compdef sshci=ssh-copy-id

# aliases
sudo() my_sudo $@
alias sudo="sudo "
pkexec() command pkexec env $@
alias pkexec="pkexec "
alias pke="pkexec "

if [ -n "$FAKEROOTKEY" ]; then
	alias sudo='__fakeroot_msg sudo'
	alias su='__fakeroot_msg su'
	alias pkexec='__fakeroot_msg pkexec'
fi

if [[ -s "/etc/grc.zsh" ]]; then
	local GRC="command grc"
	unalias grc 2>/dev/null
	alias colourify="$GRC -es"
	source /etc/grc.zsh

	_LS=(grc --colour=on \ls --color=always --hyperlink=auto -Fh --group-directories-first)

	# journalctl + grc is too slow
	unset -f journalctl
	unset -f systemctl
	unset -f lolcat 2>/dev/null
else
	alias ps="ps aux"
	_LS=(ls --color=always --hyperlink=auto -Fh --group-directories-first)
fi

alias adbscreen="adb shell screencap -p | xclip -selection clipboard -t image/png "
alias adbscreencap="adb shell screencap -p > "
alias adbscreencapf="adb shell screencap -p > \"screen - \$(date).png\""
# alias ansible-playbook="ansible-playbook --ask-become-pass"
alias ansible-console='ANSIBLE_STDOUT_CALLBACK=default ansible-console'
alias axel='axel -a'
alias ca='noglob qalc'
alias cp='cp -P --reflink=auto'
alias cl='clear'
decrypt_pcap() {
	editcap --inject-secrets "tls,$2" "$1" "$1.dsb"
}

dfplay() {
	ansible-playbook "$_dotfilesdir/playbooks/$1" "${@:2}" -e _df_pwd="\'$(pwd)\'"
}
dfplayn() {
	ansible-navigator run "$_dotfilesdir/playbooks/$1" -e _df_pwd="\'$(pwd)\'" "${@:2}"
}

_dfplay() {
	_dfplay_name() {
		pushd "$_dotfilesdir/playbooks" > /dev/null
		_path_files -g "*.yml"
		popd > /dev/null
	}
	_arguments \
		'1:dfplay:_dfplay_name' \
		'*:ansible-playbook:"$_comps[ansible-playbook]" "$@"'
}
compdef _dfplay dfplay=ansible-playbook
compdef _dfplay dfplayn=ansible-playbook

if (( $+commands[duf] )); then
	alias df='duf'
fi

alias dir='dir -F --color=always'
alias distccmon='watch -n 0.1 '"'"'a=$(distccmon-text); echo "$a" | wc -l; echo "$a"'"'"
alias dmesg='dmesg -T --color=always'
alias E="$EDITOR"
alias sE='VISUAL="code -w" sudoedit'
alias fzf='fzf --ansi'
alias grep='noglob grep -iE'
alias hx=helix
alias jq='noglob jq'
alias lzd='lazydocker'

if (( $+commands[lsd] )); then
	_LS=(lsd --color=always --hyperlink=auto -h --group-directories-first --icon=always)
	alias lt="$_LS --tree"
	alias lta="lt -a"
	alias ltl="lt -l"
	alias ltal="lta -l"
	compdef lsd=ls
fi
alias ls="$_LS -l"
alias l="command $_LS -A"
cdl () {
	cd "$@"
	command $_LS -A
}
compdef cdl=cd

alias la="$_LS -lah"
alias ll="$_LS -l"
alias sl="ls"
alias lock='qdbus org.freedesktop.ScreenSaver /ScreenSaver Lock'

alias kns=kubens
alias ktx=kubectx
if (( $+commands[kubectl-argo-rollouts] )); then
	alias kar=kubectl-argo-rollouts
fi

if (( $+commands[kustomize] )); then
	alias kk='kustomize build --enable-helm'
elif (( $+commands[kubectl] )); then
	alias kk='kubectl kustomize build --enable-helm'
fi

if (( $+commands[kubectl] )); then
	alias kak='kubectl apply -k'
	kakh () {
		if (( $+commands[kustomize] )); then
			kustomize build --enable-helm "$@" | kubectl apply -f -
		elif (( $+commands[kubectl] )); then
			kubectl kustomize build --enable-helm "$@" | kubectl apply -f -
		fi
	}

fi

alias mc=mcli

if [ -n "$POWERLINE_SYMBOLS" ]; then
	alias mosh="mosh --server='systemd-run --scope --user mosh-server new -s -l POWERLINE_SYMBOLS=1'"
else
	alias mosh="mosh --server='systemd-run --scope --user mosh-server new -s -l POWERLINE_SYMBOLS='"
fi
alias notifytest="notify-send 'Hello world!' 'This is an example notification.'"
alias nix-shell='nix-shell --run zsh '
nixpfiles() {
	find $(nix-build '<nixpkgs>' -A "$1" --no-link)
}
alias O='open_command'
alias pac='paru'
alias pacman='pacman --color=always'
if (( $+commands[podman-compose] )); then
	compdef podman-compose=docker-compose
fi
alias pping='fping -c 1 ipv{4,6}.google.com'
# if (( $+commands[procs] )); then
	# alias psf='procs'
# else
	alias psf="ps aux | grep --color=always"
# fi
alias py2='python2'
alias py3='python3'
alias py='python'
alias ipy='ipython'
alias jpy='jupyter console'
alias j='journalctl'
alias ju='journalctl --user'
alias ptpy='ptipython'
alias bpy='bpython'
alias qalc='noglob qalc'
alias randstr='< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c'
alias rg='noglob rg'
alias sc='systemctl'
alias ssc='sudo systemctl'
alias scu='systemctl --user'
alias tparallel='parallel --fg --tmux'
alias tree='tree -C'
alias update-fc-cache='fc-cache -sv ; fc-cache-32 -sv'
alias update-fc-cache-f='fc-cache -vrs ; fc-cache-32 -vrs'
(( $+commands[nvim] )) && alias vim='nvim'
alias watch='watch -c '

alias bat="bat --map-syntax='*.conf:INI'"
alias cat='bat -pP'

zwatch () {
    IN=2
    case $1 in
        -n)
            IN=$2
            shift 2
            ;;
    esac
    printf '\033c' # clear
    CM="$*"
    LEFT="$(printf 'Every %.1f: %s' $IN $CM)"
    ((PAD = COLUMNS - ${#LEFT}))
    while :
    do
        DT=$(date)
        printf '\033c'"$LEFT%${PAD}s\n" "$HOST $(date)"
        eval "$CM"
        sleep $IN
    done
}
alias zw=zwatch

alias w3m='w3m -o ext_image_viewer=0'
alias wget='wget -q --show-progress '

# global aliases
## with STDOUT PIPE
alias -g C='| clipcopy'
alias -g CL='| column -t'
alias -g F='| fzf'
alias -g G='| grep -i --color=auto'
alias -g H='| head'
alias -g HL='| bat'
alias -g N="&> /dev/null &"
alias -g PJS='| pp_json'
alias -g P='| $PAGER'
alias -g T='| sd'
alias -g V='"$(clippaste)"'
alias -g XP='| code -'

## etc
alias wayland_env='export \
GDK_BACKEND=wayland \
QT_QPA_PLATFORM="wayland-egl;xcb" \
CLUTTER_BACKEND=wayland \
SDL_VIDEODRIVER=wayland \
ELM_DISPLAY=wl \
MOZ_ENABLE_WAYLAND=1 \
'

# compdef _grc grc

_pkill_restart() {
	pushd ~ &>/dev/null
	pkill "$1"
	"$@" &>/dev/null &
	local _retpid=$!
	popd &>/dev/null

	if [[ -n "$_retpid" ]]; then
		return 0
	fi

	return 1
}
# плазма не падает
alias kR='scu restart plasma-kwin_x11.service'
alias krR='scu stop plasma-krunner.service'
alias pR='scu restart plasma-plasmashell.service'
pwR () {
	systemctl --user stop pipewire{{,-pulse}{,.socket},-session-manager}
	systemctl --user start pipewire{{,-pulse}{,.socket},-session-manager}
	systemctl --user restart noisetorch.service
}
alias sR='_pkill_restart solaar --window=hide'
# nvidia fuck you (reload this module helps nvidia-vaapi and cuda)
alias nvfuck_uvm='sudo rmmod nvidia_uvm; sudo modprobe nvidia_uvm'
nvfuck_display() {
	echo on | sudo tee /sys/bus/pci/devices/0000\:01\:00.0/power/control
	sleep 1
	echo auto | sudo tee /sys/bus/pci/devices/0000\:01\:00.0/power/control
}

plaunch() {
	setopt LOCAL_OPTIONS NO_MONITOR
	for p in "$@"; do
		eval "$p" &
	done
	wait
}

# https://bugs.kde.org/show_bug.cgi?id=480960
# plaunch() {
# 	setopt LOCAL_OPTIONS NO_MONITOR
# 	for p in "$@"; do
# 		eval "$p"
# 		sleep 0.1
# 	done
# 	wait
# }

ycur() {
	local _diff=${1:-200}
	local _step=${2:-10}
	local _sleep=${3:-0.05}

	local _range=( $(seq 0 "$_step" "$_diff") )

	while true; do
		for i in "${_range[@]}"; do
			ydotool mousemove -- 0 "$_step"
			sleep "$_sleep"
		done
		for i in "${_range[@]}"; do
			ydotool mousemove -- 0 -"$_step"
			sleep "$_sleep"
		done
	done
}

wallpaper() {
	qdbus org.kde.plasmashell /PlasmaShell org.kde.PlasmaShell.evaluateScript \
		'
		var allDesktops = desktops();
		print (allDesktops);
		for (i = 0; i < allDesktops.length; i++) {
			d = allDesktops[i];
			d.wallpaperPlugin = "org.kde.image";
			d.currentConfigGroup = Array("Wallpaper", "org.kde.image", "General");
			d.writeConfig("Image", "'"$1"'");
		}
		'
	kwriteconfig6 --file ~/.config/kscreenlockerrc --group Greeter --group Wallpaper --group org.kde.image --group General --key Image "$1"
}

dyn_wallpaper() {
	qdbus org.kde.plasmashell /PlasmaShell org.kde.PlasmaShell.evaluateScript \
		'
		var allDesktops = desktops();
		print (allDesktops);
		for (i = 0; i < allDesktops.length; i++) {
			d = allDesktops[i];
			d.wallpaperPlugin = "com.github.zzag.dynamic";
			d.currentConfigGroup = Array("Wallpaper", "com.github.zzag.dynamic", "General");
			d.writeConfig("Image", "'"$1"'");
		}
		'
	kwriteconfig6 --file ~/.config/kscreenlockerrc --group Greeter --group Wallpaper --group com.github.zzag.dynamic --group General --key Image "$1"
}

set_wallpaper() {
	case "$1" in
		day)
			if [[ -n "$DF_DYN_WALLPAPER" ]]; then
				dyn_wallpaper "$DF_DYN_WALLPAPER"
			elif [[ -n "$DF_WALLPAPER_DAY" ]]; then
				wallpaper "$DF_WALLPAPER_DAY"
			fi
			;;
		night)
			if [[ -n "$DF_DYN_WALLPAPER" ]]; then
				dyn_wallpaper "$DF_DYN_WALLPAPER"
			elif [[ -n "$DF_WALLPAPER_NIGHT" ]]; then
				wallpaper "$DF_WALLPAPER_NIGHT"
			fi
			;;
	esac
}

tlight() {
	plaunch \
		'plasma-apply-cursortheme breeze_cursors' \
		"plasma-apply-colorscheme '${DF_PLASMA_LIGHT:-BreezeClassic}'"
		#QT_QPA_PLATFORM=offscreen /usr/lib/plasma-changeicons "${DF_PLASMA_LIGHT_ICONS:-breeze}"
}

tdark() {
	plaunch \
		'plasma-apply-cursortheme Breeze_Light' \
		"plasma-apply-colorscheme '${DF_PLASMA_DARK:-BreezeDark}'"
		#QT_QPA_PLATFORM=offscreen /usr/lib/plasma-changeicons "${DF_PLASMA_DARK_ICONS:-breeze-dark}"
}

ctemp() {
	setopt LOCAL_OPTIONS KSH_GLOB

	if [[ -n "$1" ]]; then
		case "$1" in
			+([0-9]))
				local temp="$1"
				;;
			+([0-9])k)
				local temp="$(( ${1:0:-1} * 1000 ))"
				;;
			+([0-9]).+([0-9])k)
				local temp="$(( ${1:0:-1} * 1000 ))"
				local temp="${temp%.}"
				;;
			day)
				local temp=6500
				;;
			night)
				local temp=2400
				;;
			lamp)
				local temp=4000
				;;
			*)
				echo "Usage: ctemp [day|night|lamp|<temp>|<temp>k]"
				return 1
				;;
		esac

		echo $temp

		if ! [[ $temp -le 6500 && $temp -ge 1000 ]]; then
			echo "Temperature must be between 1000 and 6500"
			return 1
		fi

		kwriteconfig6 --file kwinrc --group NightColor --key NightTemperature "$temp"
		gdbus emit --session -o /kwinrc -s org.kde.kconfig.notify.ConfigChanged "{'NightColor':[b'NightTemperature']}"

	else
		busctl --user get-property org.kde.KWin /ColorCorrect org.kde.kwin.ColorCorrect targetTemperature | awk '{ print $2 }'
	fi
}

clamp () {
	ctemp lamp
}
cday () {
	ctemp day
}
cnight () {
	ctemp night
}


ilight() {
	qdbus6 local.org_kde_powerdevil /org/kde/Solid/PowerManagement/Actions/BrightnessControl setBrightness "$1"
}

# ddcutil setvcp 14 01 = sRGB mode
# ddcutil setvcp 14 05 = 6500K mode (the screen is configured to use min brightness in this mode)
blday() {
	plaunch \
		'ddcutil setvcp 14 01' \
		'ilight 40'
}

blnight() {
	plaunch \
		'ddcutil setvcp 14 05' \
		'ilight 10'
}

bllamp() {
	plaunch \
		'ddcutil setvcp 14 01' \
		'ilight 25'
}

day() {
	plaunch \
		tlight \
		blday \
		'set_wallpaper day' \
		cday
}

night() {
	plaunch \
		tdark \
		blnight \
		'set_wallpaper night' \
		cnight
}

lamp() {
	plaunch \
		tdark \
		bllamp \
		'set_wallpaper night' \
		cnight
}
