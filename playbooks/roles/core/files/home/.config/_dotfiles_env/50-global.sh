#!/usr/bin/env bash

# Also add the following to ~/.ssh/config to fix powerline symbols in ssh console
## Host *
##    SendEnv LANG
[[ "$TERM" != linux ]] && export POWERLINE_SYMBOLS=1

export SSH_ASKPASS=systemd-ask-password
# export SUDO_ASKPASS='systemd-ask-password'

EDITOR="micro"
# EDITOR="helix"

if [[ "$TERM_PROGRAM" == 'vscode' ]]; then
	EDITOR="code -w"
fi

if [[ -z "$COLORTERM" && "$TERM" =~ (.*)-256color ]]; then
	# export TERM=$match[1]-truecolor
	export COLORTERM=truecolor
fi

export EDITOR
export VISUAL="$EDITOR"

export PAGER="less"
# export PAGER="bat --style=header,grid"
export SYSTEMD_PAGER="$PAGER"
# export PAGER=cat
export LESS='-RSF'
export LESS_TERMCAP_mb=$'\E[1;31m'     # begin bold
export LESS_TERMCAP_md=$'\E[1;36m'     # begin blink
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m'        # reset underline
export BAT_PAGER="$PAGER $LESS"

if command -v fd &> /dev/null; then
	export FZF_DEFAULT_COMMAND='fd --strip-cwd-prefix --hidden --follow --exclude .git'
else
	export FZF_DEFAULT_COMMAND='find --strip-cwd-prefix --hidden --follow --exclude .git'
fi
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

_JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true'
# (archwiki) Note: Enabling this option may cause the UI of software like JetBrains IDEs misbehave, making them drawing windows, popups and toolbars partially.
#_JAVA_OPTIONS+=' -Dsun.java2d.opengl=true'
_JAVA_OPTIONS+=' -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel'
export _JAVA_OPTIONS

export _JAVA_AWT_WM_NONREPARENTING=1
export SAL_USE_VCLPLUGIN=kf6
export FT2_SUBPIXEL_HINTING=2 # Default mode

export GST_VAAPI_ALL_DRIVERS=1

export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
# breaks firefox with webrender on nvidia
# export GTK_CSD=1
# set it in /etc/envrionment if needed for something but plasma
if [[ "$XDG_CURRENT_DESKTOP" == "KDE" ]]; then
#&& [[ "$XDG_SESSION_TYPE" != 'wayland' ]]; then
	export GTK_USE_PORTAL=1
	# NOTE: if gtk3 is fucked up see https://bugs.kde.org/show_bug.cgi?id=474746
	# export QT_QPA_PLATFORMTHEME=xdgdesktopportal  # breaks portals in some apps like yuzu
else
	unset GTK_USE_PORTAL
fi

export SDL_VIDEO_MINIMIZE_ON_FOCUS_LOSS=0

if [[ "$XDG_SESSION_TYPE" == 'wayland' ]]; then
	# breaks X11 ff87 if enabled in X11
	export MOZ_ENABLE_WAYLAND=1
	export CLUTTER_BACKEND=wayland
	#export QT_QPA_PLATFORM="wayland;xcb"
	export SDL_VIDEODRIVER="wayland,x11"
    export ELM_DISPLAY=wl
else
	export MOZ_X11_EGL=1
	export MOZ_USE_XINPUT2=1
fi

export MOZ_TELEMETRY_REPORTING=0
export MOZ_DBUS_REMOTE=1

# export PLASMA_PRELOAD_POLICY=aggressive
#export PLASMA_USE_QT_SCALING=1

export DOTNET_CLI_TELEMETRY_OPTOUT=1 # MS fuck you

# helped with dolphin sftp "authentication failed" quirk
export KDE_FORK_SLAVES=1

export DOCKER_BUILDKIT=1
export COMPOSE_DOCKER_CLI_BUILD=1

export WINEDLLOVERRIDES="mshtml=,winemenubuilder.exe=d"
export WINEARCH=win64

# export KWIN_OPENGL_INTERFACE=egl

if [[ -n "$XDG_CURRENT_DESKTOP" && "$XDG_CURRENT_DESKTOP" != "KDE" ]]; then
	export QT_QPA_PLATFORMTHEME="qt5ct"
fi

if [[ -z "$SSH_AUTH_SOCK" || "$SSH_AUTH_SOCK" =~ ^/run/user/[0-9]+/keyring/ssh$ ]]; then
	export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/ssh-agent.socket"
fi
export GSM_SKIP_SSH_AGENT_WORKAROUND=1

# export ANSIBLE_NOCOWS=1
# export ANSIBLE_STDOUT_CALLBACK=yaml
# export ANSIBLE_LOAD_CALLBACK_PLUGINS=1
# this may break ansible when requiretty sudo option is set
# export ANSIBLE_PIPELINING=1

export ANSIBLE_NAVIGATOR_EXECUTION_ENVIRONMENT=False
export ANSIBLE_NAVIGATOR_PLAYBOOK_ARTIFACT_ENABLE=False
export ANSIBLE_NAVIGATOR_LOG_FILE=/dev/null

#export LUTRIS_ENABLE_PROTON=1

# vscode shell integration fix
export ITERM_SHELL_INTEGRATION_INSTALLED=Yes

export RADV_PERFTEST=gpl
export AMD_DEBUG=useaco
#unset RADV_PERFTEST
export ZINK_DESCRIPTORS=db
export NOUVEAU_USE_ZINK=1
