#!/usr/bin/env zsh

_load_env() {
	# set -x
	for env in ~/.config/_dotfiles_env/*.sh; do
		source "$env"
	done

	if [[ -z "$_dotfiles_path_installed" && -n "${_srcdir}" ]]; then
		_upath="${HOME}/.local/bin":"${_srcdir}/bin":"${_srcdir}/zsh":"${_srcdir}/vendor":"${_srcdir}/vendor/sergrep:${KREW_ROOT:-$HOME/.krew}/bin:$HOME/.nix-profile/bin"
		PATH="${_upath}":"$PATH"
		export PATH

		export _dotfiles_path_installed=1

		#if command -v dbus-update-activation-environment &>/dev/null; then
		#	dbus-update-activation-environment --systemd --all
		#fi
	fi

	# set +x
}

_load_env
