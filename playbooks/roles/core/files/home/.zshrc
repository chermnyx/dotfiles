#!/bin/zsh

# zmodload zsh/zprof
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.

# if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  # source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
# fi

source ~/.profile

# Path to your oh-my-zsh installation.
# ZSH_CUSTOM="${_srcdir}/zsh/oh-my-zsh"

### OMZ
CASE_SENSITIVE=true
COMPLETION_WAITING_DOTS=false
DISABLE_AUTO_UPDATE=true

ZSH_TMUX_AUTOCONNECT=true
ZSH_TMUX_AUTOQUIT=false
ZSH_TMUX_FIXTERM=false  # breaks tmux with zinit
ZSH_TMUX_UNICODE=true
DISABLE_MAGIC_FUNCTIONS=true

ZSH_TMUX_AUTOSTART_ONCE=false
ZSH_TMUX_AUTOSTART=false

if [[ -z "$TMUX" ]]; then
	if [[ -n "$SSH_CONNECTION"  && "$TERM_PROGRAM" != "vscode" ]] \
		|| [[ "$COLORTERM" == "kmscon" ]]; then
		ZSH_TMUX_AUTOSTART=true
	fi
fi

DISABLE_FZF_AUTO_COMPLETION="false"
FZF_COMPLETION_TRIGGER="**"

ZSH_CACHE_DIR=$HOME/.cache/zsh
if [[ ! -d $ZSH_CACHE_DIR ]]; then
	mkdir -p $ZSH_CACHE_DIR
fi
### /OMZ


if (( $+commands[bat] )); then
	READNULLCMD=bat
else
	READNULLCMD=cat
fi

# ZLE_RPROMPT_INDENT=0

export HISTSIZE=1000000000
export SAVEHIST=$HISTSIZE
setopt EXTENDED_HISTORY

# /ENV


#[[ "$UID" == 0 ]] && umask 022 || umask 077

# source "${_srcdir}/zgen_init.zsh"
source "${_srcdir}/zinit_init.zsh"

setopt extended_glob

autoload -U zmv

# auto disown bg jobs
setopt NO_HUP
setopt NO_CHECK_JOBS

# Appends every command to the history file once it is executed
setopt inc_append_history
# Reloads the history whenever you use it
setopt share_history

second_p_color=004
second_p_text_color=015
export PROMPT2="$(print -Pn %K{$second_p_color}%F{$second_p_text_color} '%_' %F{$second_p_color}%k"\uE0B0"%f)"
export PROMPT3="$(print -Pn %K{$second_p_color}%F{$second_p_text_color} '?)' %F{$second_p_color}%k"\uE0B0"%f) "
export PROMPT4="$(print -Pn %K{$second_p_color}%F{$second_p_text_color} '%N: %i' %F{$second_p_color}%k"\uE0B0"%f) "
export SUDO_PROMPT="$(print -Pn %K{005}%F{015} "$(print_icon ROOT_ICON)" 'password for %%p' %F{005}%k"\uE0B0"%f) "

# (( ! ${+functions[p10k]} )) || p10k finalize
# zprof
