#!/bin/zsh

source ~/.profile

if [[ $SSH_CONNECTION == '' ]]; then
	if [[ $TERM == linux ]]; then
		# setfont ter-v14n
		if [[ $XTERM_SCREEN == Y ]] || [[ $XTERM_SCREEN == y ]]; then
			exec startx -- :3
		else
			echo -en "]P0000000" #black
			echo -en "]P8616161" #darkgrey
			echo -en "]P1b71c1c" #darkred
			echo -en "]P9f44336" #red
			echo -en "]P264dd17" #darkgreen
			echo -en "]PA76ff03" #green
			echo -en "]P3f57c00" #brown
			echo -en "]PBffeb3b" #yellow
			echo -en "]P4131c61" #darkblue
			echo -en "]PC2196f3" #blue
			echo -en "]P5aa00ff" #darkmagenta
			echo -en "]PDe040fb" #magenta
			echo -en "]P600bcd4" #darkcyan
			echo -en "]PE00e5ff" #cyan
			echo -en "]P7ffffff" #lightgrey
			echo -en "]PFffffff" #white
			stty pass8
			clear
		fi
	fi
fi
