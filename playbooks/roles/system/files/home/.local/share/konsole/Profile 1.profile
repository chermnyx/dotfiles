[Appearance]
BorderWhenActive=false
ColorScheme=Breeze
Font=JetBrainsMonoNL NFM,10,-1,5,50,0,0,0,0,0
UseFontLineChararacters=false

[Cursor Options]
CursorShape=1
UseCustomCursorColor=false

[General]
Command=/bin/zsh
DimWhenInactive=false
Environment=TERM=xterm-256color,POWERLINE_SYMBOLS=1
InvertSelectionColors=true
LocalTabTitleFormat=%w
Name=Profile 1
Parent=FALLBACK/
RemoteTabTitleFormat=(%w)
TerminalCenter=false
TerminalColumns=128
TerminalMargin=0
TerminalRows=32

[Interaction Options]
ColorFilterEnabled=true
MouseWheelZoomEnabled=false
TrimTrailingSpacesInSelectedText=true
UnderlineFilesEnabled=true
UnderlineLinksEnabled=true
WordCharacters=@-./_~?&=%+#

[Keyboard]
KeyBindings=default

[Scrolling]
HistoryMode=1
HistorySize=8192
ReflowLines=true

[Terminal Features]
BlinkingCursorEnabled=true
FlowControlEnabled=true
UrlHintsModifiers=0
