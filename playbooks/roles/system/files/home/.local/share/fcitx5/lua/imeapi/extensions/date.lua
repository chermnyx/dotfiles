function date(input)
  return os.date("%Y-%m-%d")
end

ime.register_trigger("date", "date", { "date" }, {})

function time(input)
  return os.date("%H:%M:%S")
end

ime.register_trigger("time", "time", { "time" }, {})


function datetime(input)
  return os.date("%Y-%m-%d %H:%M:%S")
end

ime.register_trigger("datetime", "datetime", { "datetime" }, {})

function ip(input)
  local fd = io.popen("curl https://ifconfig.me/ 2>/dev/null")
  local result = fd:read("*all")
  fd:close()
  return result
end

ime.register_trigger("ip", "ip", { "ip" }, {})
